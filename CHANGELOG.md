<!--
SPDX-FileCopyrightText: 2023 David Runge <dave@sleepmap.de>
SPDX-License-Identifier: CC-BY-SA-4.0
-->
- - -
## 0.1.0 - 2023-09-06
#### Continuous Integration
- Add GitLab CI integration for linting, building and testing. - (7895300) - David Runge
- Add Github CI integration for linting, building and testing. - (d5843aa) - David Runge
#### Features
- Add cargo-deny configuration - (8ead539) - David Runge
- Add config for codespell - (74957f6) - David Runge

- - -


